const express = require('express')
const application = express()
const port = 5005;

function Router() {
    application.get('/', (req, res) => {
        res.send('Teste')
    })
    
    application.listen(port, () => {
        console.log("Running system on port: " + port)
    })
}

module.exports = Router();
